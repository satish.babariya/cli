import chalk from "chalk";
import path from "path";
import { Command } from "commander";
import packageJson from "../package.json";

//@ts-ignore
import envinfo from "envinfo";

const program = new Command();

program.name(packageJson.name);
program.version(packageJson.version, "-v, --version", "resty.js cli version");

program
  .command("create <project-name> [options]")
  .usage(`${chalk.green("<project-name>")} [options]`)
  .description("create new resty.js project")
  .action(async (name, options) => {
    console.log("clone command called", name, options);
    envinfo
      .run(
        {
          System: ["OS", "CPU"],
          Binaries: ["Node", "npm", "Yarn"],
        },
        {
          duplicates: true,
          showNotFound: true,
        }
      )
      .then(console.log);
  })
  .option("--verbose", "print additional logs");

// program
//   .command("create <project-name>")
//   //   .arguments("<project-name>")
//   .usage(`${chalk.green("create <project-name>")} [options]`)
//   .action((name) => {
//     projectName = name;
//   })
//   .option("--verbose", "print additional logs")
//   .option("--info", "print environment debug info")
//   .option(
//     "--template <path-to-template>",
//     "specify a template for the created project"
//   )
//   //   .option("--use-npm")
//   .allowUnknownOption()
//   .parse(process.argv);

program.parse(process.argv);
